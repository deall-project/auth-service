# Application Name: Auth Service

# DEVELOPER: Muhammad Ary Widodo

# How to run

```bash
$ cd auth-service
$ npm i
$ npm start
```

# API List

TRAVELLER API

| Routes | EndPoint                         | Description                                            |
| ------ | ---------------------------------| ------------------------------------------------------ |
| POST   | /api/auth/register               | Register user (input requirement:        |
|        |                                  | username, password and level(optional))                |
| POST   | /api/auth/login                  | Get Token & Login  with (username and password)|

```
POST
/api/user/registrasi
username: String
password: String
level: String (Optional)


POST
/api/user/login
(GENERATE TOKEN)
username: String
password: String
