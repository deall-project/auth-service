"use strict"

const User = require('../models/user.model')
const bcryptLib = require('../libraries/bcrypt.lib')
const jwtLib = require('../libraries/jwt.lib')
const authSchema = require('../libraries/validation.lib')

let authController = {
    register: async (req, res) => {
        try {
            let payload = await authSchema.validateAsync(req.body) // validasi payload
            let { username, password, level = "user"} = payload

            let user_exist = await User.findOne({ where: { username } })
            if(user_exist) throw new Error('Username sudah terdaftar!')

            password = bcryptLib.hasher(password)

            let registered = await User.create({
                username, password, level
            })
            if(!registered) throw new Error('Gagal mendaftar')

            res.status(201).json({
                status: 'Success',
                message: 'User created successfully'
            })
        } catch (error) {
            res.status(401).json({
                status: 'Failed',
                message: error.message
            })
        }
    },
    login: async (req, res) => {
        try {
            let payload = await authSchema.validateAsync(req.body) // validasi payload
            let { username, password} = payload

            let user_exist = await User.findOne({ where:{username}})
            if(!user_exist) throw new Error('Username atau password anda salah!')

            // validate password
            let validated = bcryptLib.checker(password, user_exist.dataValues.password)
            if(!validated) throw new Error('Username atau password salah!')

            // generate token
            let token = jwtLib.generate({
                id: user_exist.dataValues.id,
                username: user_exist.dataValues.username,
                level: user_exist.dataValues.level
            })

            res.status(200).json({
                status: 'Success',
                token
            })
        } catch (error) {
            res.status(401).json({
                status: 'Failed',
                message: error.message
            })
        }
    }
}

module.exports = authController