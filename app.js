"use strict"

const express = require('express')
    , config = require('./config')
    , authRoute = require('./routes/auth.route')
    , port = config.port
    , app = express()

app.use(express.json())

app.use('/api', authRoute)

app.get('/', (req, res) => {
    res.send('Wellcome to Quick_count API')

})
app.all('*', (req, res) => {
    res.send('Are you lost ?')
})

app.listen(port, () => {
    console.log(`Server running on port: ${port}`)
})