"use strict"

const jwt = require('jsonwebtoken')
const config = require('../config')

let jwtLib = {
    // generate: jwt.sign(data, config.token_secret, { expiresIn: 3600}),
    // compare: jwt.verify(token, config.token_secret)
    generate: (data) => {
        return jwt.sign(data, config.token_secret, {expiresIn:3600})
    },
    verify: (token) => {
        return jwt.verify(data, config.token_secret)
    }
}

module.exports = jwtLib